const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const eventSchema = new mongoose.Schema({
  name: String,
  startTime: Date
}, {
  timestamps: true
})

module.exports = mongoose.model('Event', eventSchema)
