import React from 'react'
import { shallow } from 'enzyme'
import toJSON from 'enzyme-to-json'
import Event from '../components/Event'

describe('<Event />', () => {
  const fakeEvent = {
    _id: '123ABC',
    name: 'Fake event',
    startTime: new Date("11 January, 2011")
  }

  const wrapper = shallow(<Event event={fakeEvent}/>);

  it('renders without throwing errors', () => {
    expect(wrapper.exists()).toBe(true)
    expect(wrapper.exists('.event-name')).toBe(true)
    expect(wrapper.exists('.event-time')).toBe(true)
  })

  it('renders and matches snapshot', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
  })
});
