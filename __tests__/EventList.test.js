import React from 'react'
import { shallow } from 'enzyme'
import toJSON from 'enzyme-to-json'
import Event from '../components/Event'
import EventList from '../components/EventList'

describe('<EventList />', () => {
  const fakeEvent = {
    _id: '123ABC',
    name: 'Fake event',
    startTime: new Date("11 January, 2011")
  }
  const fakeEventList = [fakeEvent]
  const wrapper = shallow(<EventList events={fakeEventList}/>);

  it('renders child Event without throwing errors', () => {
    expect(wrapper.exists('Event')).toBe(true)
    expect(wrapper.find('Event').props().event).toBe(fakeEvent)
  })

  it('renders and matches snapshot', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
  })
});
