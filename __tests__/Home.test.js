import React from 'react'
import { shallow } from 'enzyme'
import toJSON from 'enzyme-to-json'
import Home from '../pages/index'

describe('<Home />', () => {
  const wrapper = shallow(<Home/>);

  it('renders without throwing errors', () => {
    expect(wrapper.exists('EventContainer')).toBe(true)
  })

  it('renders and matches snapshot', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
  })
});
