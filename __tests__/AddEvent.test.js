import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';

import AddEvent from '../components/AddEvent';


describe('<AddEvent />', () => {
  const props = {
    onSubmit: (e) => (e),
    onEventNameChange: (e) => (e),
    onDateChange: (e) => (e),
    eventStartDateTime: new Date(0),
    eventName: "some name",
    toggleHidePastEvents: (e) => (e)
  }

  const wrapper = shallow(<AddEvent {...props}/>);

  it('should render without throwing an error', () => {
    expect(wrapper.exists("form.add-event-form")).toBe(true)
   })

  it('renders and matches snapshot', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
  })
});

