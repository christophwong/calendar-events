import React from 'react'
import { shallow } from 'enzyme'
import toJSON from 'enzyme-to-json'
import EventContainer from '../components/EventContainer'
import AddEvent from '../components/AddEvent'
import EventList from '../components/EventList'

describe('<EventContainer />', () => {
  const fakeEvent = {
    _id: '123ABC',
    name: 'Fake event',
    startTime: new Date("11 January, 2011")
  }
  const fakeEventList = [fakeEvent]
  const wrapper = shallow(<EventContainer events={fakeEventList}/>);

  it('renders child EventList and AddEvent form without throwing errors', () => {
    expect(wrapper.exists('AddEvent')).toBe(true)
    expect(wrapper.find('EventList').props().events).toBe(fakeEventList)
  })

  it('set state based on changeHandler in child', () => {
    const dummyValue = "dummyValue"
    const dateValue = new Date(0)
    wrapper.find('AddEvent').props().onEventNameChange({target: {value: dummyValue}})
    wrapper.find('AddEvent').props().onDateChange(dateValue)
    expect(wrapper.state('eventName')).toBe(dummyValue)
    expect(wrapper.state('eventStartDateTime').toLocaleString()).toBe(dateValue.toLocaleString())
  })

  it('renders and matches snapshot', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
  })
});
