import React from 'react'
import { shallow } from 'enzyme'
import toJSON from 'enzyme-to-json'
import HidePastEvents from '../components/HidePastEvents'

describe('<HidePastEvents />', () => {
  const wrapper = shallow(<HidePastEvents/>);

  it('renders without throwing errors', () => {
    expect(wrapper.exists('button')).toBe(true)
  })

  it('renders and matches snapshot', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
  })

  it('displays Show when past events are HIDDEN', () => {
    const hidePastEventsWrapper = shallow(<HidePastEvents hidePastEvents={true}/>);
    expect(hidePastEventsWrapper.text()).toContain('Show')
  })

  it('displays Hide when past events are SHOWING', () => {
    const hidePastEventsWrapper = shallow(<HidePastEvents hidePastEvents={false}/>);
    expect(hidePastEventsWrapper.text()).toContain('Hide')
  })

});
