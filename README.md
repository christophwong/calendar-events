# Calendar-events

A simple web application that stores and retrieves calendar events.

## Up & Running with Terminal Commands

1. `git clone git@gitlab.com:christophwong/calendar-events.git`
2. `cd calendar-events`
3. `npm install`
4. `mongod` (I usually like to run this in a different bash session)
   (docker alternative: run `docker run -d -p 27017:27017 --name localmongo mongo:4.0.4`
5. `npm run dev`
6. `open http://localhost:3001`

## Tests

Run test by using `npm test` command
