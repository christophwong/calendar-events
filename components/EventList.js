import Event from './Event'

const EventList = props => (
  <div>
    <h3>Events:</h3>
    <ul>
      {props.events.map(event => (
        <Event key={event._id} event={event} hidePastEvents={props.hidePastEvents}/>
        ))}
    </ul>
    <style jsx>
      {`
        h3 {
          color: rgb(0, 76, 143);
        }
        div {
          box-shadow: 0 0 5px 3px rgba(0, 0, 0, 0.05);
          background: rgba(0, 0, 0, 0.02);
          border: 5px solid white;
          padding: 20px;
          font-size: 1rem;
          line-height: 1.5;
          font-weight: 400;
        }
        ul {
          list-style: none;
        }
        `} </style>
        </div>
        )

export default EventList