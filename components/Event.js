export default class Event extends React.Component {
  constructor (props){
    super(props)
    this.state = {
      isHidden: (this.props.hidePastEvents && this.isStartTimeEarlierThanNow(this.props.event)),
      event: this.props.event
    }
  }

  isStartTimeEarlierThanNow = (event) => {
    const startTime = new Date(event.startTime)
    const now = new Date()
    return startTime < now
  }

  setPastEventState = (event) => {
    this.setState({
      isHidden: this.props.hidePastEvents && this.isStartTimeEarlierThanNow(event)
    })
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.setPastEventState(this.props.event),
      1000
      );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  render () {
    const event = this.props.event

    return (!this.state.isHidden &&
      <li>
        <p className="event-name">
          {event.name}
        </p>
        <p className="event-time">{event.startTime && new Date(event.startTime).toLocaleString()}</p>
        <style jsx>
          {`
            li {
              background: white;
              border: 1px solid rgb(250, 250, 250);;
              box-shadow: rgb(20, 20, 20);;
              position: relative;
              display: flex;
              flex-direction: column;
            }
            .event-name {
              line-height: 2;
              font-weight: 300;
              padding: 0 3rem;
              font-size: 1.5rem;
            }

            .event-time {
              line-height: 2;
              font-weight: 300;
              padding: 0 4rem;
              font-size: 1rem;
            }

            `}
          </style>
          </li>
          )
  }
}