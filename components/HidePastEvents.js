const HidePastEvents = props => (
  <button onClick={props.onClick}>
    {props.hidePastEvents ? 'Show' : 'Hide'} Past Events
    <style jsx> {`
      button {
        margin-top: 0.5rem;
        background-color: rgb(0, 76, 143);
        color: rgb(250, 250, 250);
        width: auto;
        display: block;
        border: 0;
        font-size: 1rem;
        font-weight: 600;
        padding: 0.5rem 1.2rem;
      }
      `}</style>
      </button>
      )

export default HidePastEvents