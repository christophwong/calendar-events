import fetch from 'isomorphic-unfetch'
import AddEvent from './AddEvent'
import EventList from './EventList'

export default class EventContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      events: this.props.events,
      hidePastEvents: true
    }
  }

  onEventNameChange = (e) => {
    const eventName = e.target.value
    this.setState({eventName: eventName})
  }

  onDateChange = (dateInput) => {
    this.setState({eventStartDateTime: new Date(dateInput)})
  }

  onSubmit = async (e) => {
   e.preventDefault();
   const eventName = this.state.eventName
   const startTime = this.state.eventStartDateTime
   const res = await fetch('http://localhost:3001/api/events', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ 
      eventName,
      startTime
    })
  }).then( response => (
    response.json()
    )).then(event => {
    const events = [...this.state.events, event].sort((a, b) => (a.startTime > b.startTime))
    this.setState(state => ({
      events: events,
      eventName: "",
      eventStartDateTime: null
    }))
  })
  }

  toggleHidePastEvents = (e) => {
    e.preventDefault()
    this.setState(state =>
      ({
        hidePastEvents: !this.state.hidePastEvents
      }))
  }

  render() {
    return (
      <>
        <AddEvent
         onSubmit={this.onSubmit}
         onEventNameChange={this.onEventNameChange} 
         onDateChange={this.onDateChange}
         eventStartDateTime={this.state.eventStartDateTime}
         eventName={this.state.eventName}
         toggleHidePastEvents={this.toggleHidePastEvents}
         hidePastEvents={this.state.hidePastEvents}
        />
           <EventList
            events={this.state.events}
            hidePastEvents={this.state.hidePastEvents} />
            </>
            )
  }
}