import css from 'styled-jsx/css'
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css"
import HidePastEvents from './HidePastEvents'

export default class AddEvent extends React.Component {
  render () {

    const { className, styles } = css.resolve`
    input{
      width: auto;
      padding: 0.5rem;
      font-size: 0.8rem;
      border: 1px solid black;
    }
    `

    return (
      <form onSubmit={this.props.onSubmit} className="add-event-form">
        <div>
          <input className={className} onChange={this.props.onEventNameChange} value={this.props.eventName || ""} type="text" name="eventName" placeholder="Event Name"/>
        </div>
        <DatePicker
          className={className}
          selected={this.props.eventStartDateTime}
          onChange={this.props.onDateChange}
          dateFormat="MMMM dd, YYYY h:mm aa"
          todayButton={"Today"}
          placeholderText="Start Date and time"

          showTimeSelect
          timeFormat="h:mm aa"
          timeIntervals={15}
        />
        <button>Submit</button>
        <HidePastEvents
          onClick={this.props.toggleHidePastEvents}
          hidePastEvents={this.props.hidePastEvents}/>
        {styles}
        <style jsx>{`
         form {
          box-shadow: 0 0 5px 3px rgba(0, 0, 0, 0.05);
          background: rgba(0, 0, 0, 0.02);
          border: 5px solid white;
          padding: 20px;
          font-size: 1.5rem;
          line-height: 1.5;
          font-weight: 600;
        }
        button {
          background-color: rgb(0, 76, 143);
          color: rgb(250, 250, 250);
          margin-top: 0.5rem;
          width: auto;
          display: block;
          border: 0;
          border-radius: 2px;
          font-size: 1rem;
          font-weight: 600;
          padding: 0.5rem 1.2rem;
        }
        `}</style>
        </form>
        )
  }
}