const express = require("express");
const next = require("next");
const mongoose = require("mongoose");

const Event = require("./models/Event");

const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();
const LOCAL_DB = "cal-next";
const MONGODB_URI =
  process.env.MONGODB_URI || `mongodb://localhost/${LOCAL_DB}`;
const PORT = process.env.PORT || 3001;

mongoose.connect(MONGODB_URI);

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
app
  .prepare()
  .then(() => {
    const server = express();

    // Expose MongoDB
    server.use((req, res, next) => {
      req.db = db;
      next();
    });

    server.use(express.json());
    server.use(express.urlencoded({ extended: false }));

    server.get("/api/events", (req, res) => {
      const eventQueryCallback = (err, result) => {
        if (err) throw err;
        return res.send(result);
      };

      Event.find().sort({ startTime: 1 }).exec(eventQueryCallback);
    });

    server.post("/api/events", (req, res) => {
      const { eventName, startTime } = req.body;
      const newEvent = new Event({
        name: eventName,
        startTime: startTime,
      });

      newEvent.save(function (err, event) {
        if (err) throw err;
        return res.status(201).json(event);
      });
    });

    server.get("*", (req, res) => {
      return handle(req, res);
    });

    server.listen(PORT, (err) => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${PORT}`);
    });
  })
  .catch((ex) => {
    console.error(ex.stack);
    process.exit(1);
  });
