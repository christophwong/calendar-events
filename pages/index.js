import fetch from 'isomorphic-unfetch'
import EventContainer from '../components/EventContainer'

export default class Home extends React.Component {
  static async getInitialProps({ req }) {
    const res = await fetch('http://localhost:3001/api/events')
    const json = await res.json()
    return { events: json }
  }

  render() {
    return (
      <div className="home-container">
        <h1>Try and add some events!</h1>
        <EventContainer events={this.props.events}/>  
        <style jsx>{`
          .home-container {
            margin: 0;
            padding: 0;
            border: 0;
            width: 800px;
            margin-left: auto;
            margin-right: auto;
          }
          div {
            font-family: 'Helvetica', 'sans-serif';
          }
          h1 {
            color: rgb(0, 76, 143);
          }

          `}</style>
          </div>
          )
  }
}
